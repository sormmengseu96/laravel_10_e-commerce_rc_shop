<footer>
    <div class="container">
        <div class="footer-top">
            <div class="row">
                <div class="col-sm-6 col-md-3 col-xs-b30 col-md-b0">
                    <img src="img/logo-1.png" alt="" />
                    <div class="empty-space col-xs-b20"></div>
                    <div class="simple-article size-2 light fulltransparent">Integer posuere orci sit amet feugiat pellent que. Suspendisse vel tempor justo, sit amet posuere orci dapibus auctor</div>
                    <div class="empty-space col-xs-b20"></div>
                    <div class="footer-contact"><i class="fa fa-mobile" aria-hidden="true"></i> contact us: <a href="tel:+35235551238745">+3  (523) 555 123 8745</a></div>
                    <div class="footer-contact"><i class="fa fa-envelope-o" aria-hidden="true"></i> email: <a href="mailto:office@exzo.com">office@exzo.com</a></div>
                    <div class="footer-contact"><i class="fa fa-map-marker" aria-hidden="true"></i> address: <a href="#">1st, new york, usa</a></div>
                </div>
                <div class="col-sm-6 col-md-3 col-xs-b30 col-md-b0">
                    <h6 class="h6 light">queck links</h6>
                    <div class="empty-space col-xs-b20"></div>
                    <div class="footer-column-links">
                        <div class="row">
                            <div class="col-xs-6">
                                <a href="#">home</a>
                                <a href="#">about us</a>
                                <a href="#">products</a>
                                <a href="#">services</a>
                                <a href="#">blog</a>
                                <a href="#">gallery</a>
                                <a href="#">contact</a>
                            </div>
                            <div class="col-xs-6">
                                <a href="#">privacy policy</a>
                                <a href="#">warranty</a>
                                <a href="#">login</a>
                                <a href="#">registration</a>
                                <a href="#">delivery</a>
                                <a href="#">pages</a>
                                <a href="#">our stores</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clear visible-sm"></div>
                <div class="col-sm-6 col-md-3 col-xs-b30 col-sm-b0">
                    <h6 class="h6 light">some posts</h6>
                    <div class="empty-space col-xs-b20"></div>
                    <div class="footer-post-preview clearfix">
                        <a class="image" href="#"><img src="img/thumbnail-1.jpg" alt="" /></a>
                        <div class="description">
                            <div class="date">apr 07 / 15</div>
                            <a class="title">Fusce tincidunt accumsan pretium sit amet</a>
                        </div>
                    </div>
                    <div class="footer-post-preview clearfix">
                        <a class="image" href="#"><img src="img/thumbnail-2.jpg" alt="" /></a>
                        <div class="description">
                            <div class="date">apr 07 / 15</div>
                            <a class="title">Fusce tincidunt accumsan pretium sit amet</a>
                        </div>
                    </div>
                    <div class="footer-post-preview clearfix">
                        <a class="image" href="#"><img src="img/thumbnail-3.jpg" alt="" /></a>
                        <div class="description">
                            <div class="date">apr 07 / 15</div>
                            <a class="title">Fusce tincidunt accumsan pretium sit amet</a>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-3">
                    <h6 class="h6 light">popular tags</h6>
                    <div class="empty-space col-xs-b20"></div>
                    <div class="tags clearfix">
                        <a class="tag">headphoness</a>
                        <a class="tag">accessories</a>
                        <a class="tag">new</a>
                        <a class="tag">wireless</a>
                        <a class="tag">cables</a>
                        <a class="tag">devices</a>
                        <a class="tag">gadgets</a>
                        <a class="tag">brands</a>
                        <a class="tag">replacements</a>
                        <a class="tag">cases</a>
                        <a class="tag">cables</a>
                        <a class="tag">professional</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-bottom">
            <div class="row">
                <div class="col-lg-8 col-xs-text-center col-lg-text-left col-xs-b20 col-lg-b0">
                    <div class="copyright">&copy; 2024 All rights reserved. Development by 
                        <a href="" class="entry open-popup" data-rel="3">Sorm Mengseu</a>
                    </div>
                    <div class="follow">
                        <a class="entry" href="#"><i class="fa fa-facebook"></i></a>
                        <a class="entry" href="#"><i class="fa fa-twitter"></i></a>
                        <a class="entry" href="#"><i class="fa fa-linkedin"></i></a>
                        <a class="entry" href="#"><i class="fa fa-google-plus"></i></a>
                        <a class="entry" href="#"><i class="fa fa-pinterest-p"></i></a>
                    </div>
                </div>
                <div class="col-lg-4 col-xs-text-center col-lg-text-right">
                    <div class="footer-payment-icons">
                        <a class="entry"><img src="img/thumbnail-4.jpg" alt="" /></a>
                        <a class="entry"><img src="img/thumbnail-5.jpg" alt="" /></a>
                        <a class="entry"><img src="img/thumbnail-6.jpg" alt="" /></a>
                        <a class="entry"><img src="img/thumbnail-7.jpg" alt="" /></a>
                        <a class="entry"><img src="img/thumbnail-8.jpg" alt="" /></a>
                        <a class="entry"><img src="img/thumbnail-9.jpg" alt="" /></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>

{{-- modal --}}
<div class="popup-wrapper">
    <div class="bg-layer"></div>
    <div class="popup-content" data-rel="1">
        <div class="layer-close"></div>
        <div class="popup-container size-1">
            <div class="popup-align">
                <h3 class="h3 text-center">Log in</h3>
                <div class="empty-space col-xs-b30"></div>
                <input class="simple-input" type="text" value="" placeholder="Your email" />
                <div class="empty-space col-xs-b10 col-sm-b20"></div>
                <input class="simple-input" type="password" value="" placeholder="Enter password" />
                <div class="empty-space col-xs-b10 col-sm-b20"></div>
                <div class="row">
                    <div class="col-sm-6 col-xs-b10 col-sm-b0">
                        <div class="empty-space col-sm-b5"></div>
                        <a class="simple-link">Forgot password?</a>
                        <div class="empty-space col-xs-b5"></div>
                        <a class="simple-link">register now</a>
                    </div>
                    <div class="col-sm-6 text-right">
                        <a class="button size-2 style-3" href="#">
                            <span class="button-wrapper">
                                <span class="icon"><img src="img/icon-4.png" alt="" /></span>
                                <span class="text">submit</span>
                            </span>
                        </a>  
                    </div>
                </div>
                <div class="popup-or">
                    <span>or</span>
                </div>
                <div class="row m5">
                    <div class="col-sm-4 col-xs-b10 col-sm-b0">
                        <a class="button facebook-button size-2 style-4 block" href="#">
                            <span class="button-wrapper">
                                <span class="icon"><img src="img/icon-4.png" alt="" /></span>
                                <span class="text">facebook</span>
                            </span>
                        </a>
                    </div>
                    <div class="col-sm-4 col-xs-b10 col-sm-b0">
                        <a class="button twitter-button size-2 style-4 block" href="#">
                            <span class="button-wrapper">
                                <span class="icon"><img src="img/icon-4.png" alt="" /></span>
                                <span class="text">twitter</span>
                            </span>
                        </a>
                    </div>
                    <div class="col-sm-4">
                        <a class="button google-button size-2 style-4 block" href="#">
                            <span class="button-wrapper">
                                <span class="icon"><img src="img/icon-4.png" alt="" /></span>
                                <span class="text">google+</span>
                            </span>
                        </a>
                    </div>
                </div>
            </div>
            <div class="button-close"></div>
        </div>
    </div>

    <div class="popup-content" data-rel="2">
        <div class="layer-close"></div>
        <div class="popup-container size-1">
            <div class="popup-align">
                <h3 class="h3 text-center">register</h3>
                <div class="empty-space col-xs-b30"></div>
                <input class="simple-input" type="text" value="" placeholder="Your name" />
                <div class="empty-space col-xs-b10 col-sm-b20"></div>
                <input class="simple-input" type="text" value="" placeholder="Your email" />
                <div class="empty-space col-xs-b10 col-sm-b20"></div>
                <input class="simple-input" type="password" value="" placeholder="Enter password" />
                <div class="empty-space col-xs-b10 col-sm-b20"></div>
                <input class="simple-input" type="password" value="" placeholder="Repeat password" />
                <div class="empty-space col-xs-b10 col-sm-b20"></div>
                <div class="row">
                    <div class="col-sm-7 col-xs-b10 col-sm-b0">
                        <div class="empty-space col-sm-b15"></div>
                        <label class="checkbox-entry">
                            <input type="checkbox" /><span><a href="#">Privacy policy agreement</a></span>
                        </label>
                    </div>
                    <div class="col-sm-5 text-right">
                        <a class="button size-2 style-3" href="#">
                            <span class="button-wrapper">
                                <span class="icon"><img src="img/icon-4.png" alt="" /></span>
                                <span class="text">submit</span>
                            </span>
                        </a>  
                    </div>
                </div>
                <div class="popup-or">
                    <span>or</span>
                </div>
                <div class="row m5">
                    <div class="col-sm-4 col-xs-b10 col-sm-b0">
                        <a class="button facebook-button size-2 style-4 block" href="#">
                            <span class="button-wrapper">
                                <span class="icon"><img src="img/icon-4.png" alt="" /></span>
                                <span class="text">facebook</span>
                            </span>
                        </a>
                    </div>
                    <div class="col-sm-4 col-xs-b10 col-sm-b0">
                        <a class="button twitter-button size-2 style-4 block" href="#">
                            <span class="button-wrapper">
                                <span class="icon"><img src="img/icon-4.png" alt="" /></span>
                                <span class="text">twitter</span>
                            </span>
                        </a>
                    </div>
                    <div class="col-sm-4">
                        <a class="button google-button size-2 style-4 block" href="#">
                            <span class="button-wrapper">
                                <span class="icon"><img src="img/icon-4.png" alt="" /></span>
                                <span class="text">google+</span>
                            </span>
                        </a>
                    </div>
                </div>
            </div>
            <div class="button-close"></div>
        </div>
    </div>
    {{-- popup --}}
    <div class="popup-content" data-rel="3">
        <div class="layer-close"></div>
        <div class="popup-container size-2">
            
            <div class="popup-align">
                <div class="row">
                    <div class="col-sm-6 col-xs-b30 col-sm-b0">
                        
                        <div class="main-product-slider-wrapper swipers-couple-wrapper">
                            <div class="swiper-container swiper-control-top">
                               <div class="swiper-button-prev hidden"></div>
                               <div class="swiper-button-next hidden"></div>
                               <div class="swiper-wrapper">
                                   <div class="swiper-slide">
                                        <div class="swiper-lazy-preloader"></div>
                                        <div class="product-big-preview-entry swiper-lazy" data-background="{{ asset('sb_admin/img/photo_2024-02-13_13-49-16.jpg ') }}"></div>
                                   </div>
                               </div>
                            </div>

                            <div class="empty-space col-xs-b30 col-sm-b60"></div>

                            <div class="swiper-container swiper-control-bottom" data-breakpoints="1" data-xs-slides="3" data-sm-slides="3" data-md-slides="4" data-lt-slides="5" data-slides-per-view="5" data-center="1" data-click="1">
                               <div class="swiper-button-prev hidden"></div>
                               <div class="swiper-button-next hidden"></div>
                               <div class="swiper-wrapper">
                                   <div class="swiper-slide">
                                        <div class="product-small-preview-entry">
                                            <img src="{{ asset('user_home/img/product-preview-4_.jpg') }}" alt="" />
                                        </div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="product-small-preview-entry">
                                            <img src="img/product-preview-5_.jpg" alt="" />
                                        </div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="product-small-preview-entry">
                                            <img src="img/product-preview-6_.jpg" alt="" />
                                        </div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="product-small-preview-entry">
                                            <img src="img/product-preview-7_.jpg" alt="" />
                                        </div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="product-small-preview-entry">
                                            <img src="img/product-preview-8_.jpg" alt="" />
                                        </div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="product-small-preview-entry">
                                            <img src="img/product-preview-9_.jpg" alt="" />
                                        </div>
                                    </div>
                                    <div class="swiper-slide">
                                        <div class="product-small-preview-entry">
                                            <img src="img/product-preview-10_.jpg" alt="" />
                                        </div>
                                   </div>

                               </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-sm-6">
                        <div class="simple-article size-3 grey col-xs-b5">Web Developer</div>
                        <div class="h3 col-xs-b25">Sorm Mengseu</div>
                        <div class="row col-xs-b25">
                            <div class="col-sm-6">
                                <div class="simple-article size-5 grey">PRICE: <span class="color">$225.00</span></div>        
                            </div>
                            <div class="col-sm-6 col-sm-text-right">
                                <div class="rate-wrapper align-inline">
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star" aria-hidden="true"></i>
                                    <i class="fa fa-star-o" aria-hidden="true"></i>
                                </div>
                                <div class="simple-article size-2 align-inline">128 Reviews</div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="simple-article size-3 col-xs-b5">ITEM NO.: <span class="grey">127-#5238</span></div>
                            </div>
                            <div class="col-sm-6 col-sm-text-right">
                                <div class="simple-article size-3 col-xs-b20">AVAILABLE.: <span class="grey">YES</span></div>
                            </div>
                        </div>
                        <div class="simple-article size-3 col-xs-b30">
                            Hello I'm Sorm Mengseu 
                        </div>
                        <div class="row col-xs-b40">
                            <div class="col-sm-3">
                                <div class="h6 detail-data-title size-1">Skill</div>
                            </div>
                            <div class="col-sm-9">
                                <select class="SlectBox">
                                    <option disabled="disabled" selected="selected">Show Skill</option>
                                    <option value="">C#</option>
                                    <option value="">Php</option>
                                    <option value="">Jquary</option>
                                    <option value="">Html</option>
                                    <option value="">Css</option>
                                    <option value="">Javascript</option>
                                    <option value="">boostrap</option>
                                    <option value="">laravel</option>
                                    <option value="">Mysql</option>
                                    <option value="">Photoshop</option>
                                    <option value="">Illustrator</option>
                                    <option value="">Indesign</option>
                                    <option value="">Microsoft team</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="button-close"></div>
        </div>
    </div>

</div> 
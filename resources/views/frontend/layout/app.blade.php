<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="format-detection" content="telephone=no" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no, minimal-ui"/>
    
    <!-- fonts -->
    <link href="https://fonts.googleapis.com/css?family=Questrial|Raleway:700,900" rel="stylesheet">

    <link href="{{ asset('/user_home/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('user_home/css/bootstrap.extension.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('user_home/css/style.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('user_home/css/swiper.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('user_home/css/sumoselect.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('user_home/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.11.3/font/bootstrap-icons.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.7.1/jquery.min.js"></script>
    
    <link rel="shortcut icon" href="img/favicon.ico" />
  	<title>RC Shop</title>
</head>
<body>

    @include('frontend.layout.nav')

    @yield('content')

    @include('frontend.layout.footer')
    @yield('js')
    <script src="{{ asset('user_home/js/jquery-2.2.4.min.js') }}"></script>
    <script src="{{ asset('user_home/js/swiper.jquery.min.js') }}"></script>
    <script src="{{ asset('user_home/js/global.js') }}"></script>

    <!-- styled select -->
    <script src="{{ asset('user_home/js/jquery.sumoselect.min.js') }}"></script>

    <!-- counter -->
    <script src="{{ asset('user_home/js/jquery.classycountdown.js') }}"></script>
    <script src="{{ asset('user_home/js/jquery.knob.js') }}"></script>
    <script src="{{ asset('user_home/js/jquery.throttle.js') }}"></script>
    
</body>
</html>
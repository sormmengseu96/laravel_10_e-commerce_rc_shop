<div class="h4 col-xs-b25">popular categories</div>
<ul class="categories-menu">
    <li>
        @if (getCategories()->IsNotEmpty())
            @foreach (getCategories() as $category )
                <a href="#">
                    {{ $category->name }}
                </a>
                <div class="toggle"></div>
                <ul>
                    {{-- <li>

                    </li>
                    <li>

                    </li> --}}
                    @foreach ($products as $product )
                        <li>
                            <a href="#">{{ $product->categoryID}}</a>
                        </li>
                        {{-- <li>
                            <a href="#">tv &amp; audio</a>
                        </li>
                        <li>
                            <a href="#">gadgets</a>
                        </li> --}}
                    @endforeach
                </ul>
            @endforeach
        @else
            No categorie found
        @endif
    </li>
    <li>
        <a href="#">video &amp; photo cameras</a>
        <div class="toggle"></div>
        <ul>
            <li>
                <a href="#">laptops &amp; computers</a>
                <div class="toggle"></div>
                <ul>
                    <li>
                        <a href="#">laptops &amp; computers</a>
                    </li>
                    <li>
                        <a href="#">video &amp; photo cameras</a>
                    </li>
                    <li>
                        <a href="#">smartphones</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="#">video &amp; photo cameras</a>
                <div class="toggle"></div>
                <ul>
                    <li>
                        <a href="#">laptops &amp; computers</a>
                    </li>
                    <li>
                        <a href="#">smartphones</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="#">smartphones</a>
            </li>
            <li>
                <a href="#">tv &amp; audio</a>
            </li>
            <li>
                <a href="#">gadgets</a>
            </li>
        </ul>
    </li>
    <li>
        <a href="#">smartphones</a>
        <div class="toggle"></div>
        <ul>
            <li>
                <a href="#">laptops &amp; computers</a>
                <div class="toggle"></div>
                <ul>
                    <li>
                        <a href="#">laptops &amp; computers</a>
                    </li>
                    <li>
                        <a href="#">video &amp; photo cameras</a>
                    </li>
                    <li>
                        <a href="#">smartphones</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="#">video &amp; photo cameras</a>
                <div class="toggle"></div>
                <ul>
                    <li>
                        <a href="#">video &amp; photo cameras</a>
                    </li>
                    <li>
                        <a href="#">smartphones</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="#">smartphones</a>
            </li>
            <li>
                <a href="#">tv &amp; audio</a>
            </li>
            <li>
                <a href="#">gadgets</a>
            </li>
        </ul>
    </li>
    <li>
        <a href="#">tv &amp; audio</a>
        <div class="toggle"></div>
        <ul>
            <li>
                <a href="#">laptops &amp; computers</a>
                <div class="toggle"></div>
                <ul>
                    <li>
                        <a href="#">laptops &amp; computers</a>
                    </li>
                    <li>
                        <a href="#">video &amp; photo cameras</a>
                    </li>
                    <li>
                        <a href="#">smartphones</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="#">video &amp; photo cameras</a>
                <div class="toggle"></div>
                <ul>
                    <li>
                        <a href="#">video &amp; photo cameras</a>
                    </li>
                    <li>
                        <a href="#">smartphones</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="#">smartphones</a>
            </li>
            <li>
                <a href="#">tv &amp; audio</a>
            </li>
            <li>
                <a href="#">gadgets</a>
            </li>
        </ul>
    </li>
    <li>
        <a href="#">gadgets</a>
        <div class="toggle"></div>
        <ul>
            <li>
                <a href="#">laptops &amp; computers</a>
                <div class="toggle"></div>
                <ul>
                    <li>
                        <a href="#">laptops &amp; computers</a>
                    </li>
                    <li>
                        <a href="#">video &amp; photo cameras</a>
                    </li>
                    <li>
                        <a href="#">smartphones</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="#">video &amp; photo cameras</a>
                <div class="toggle"></div>
                <ul>
                    <li>
                        <a href="#">video &amp; photo cameras</a>
                    </li>
                    <li>
                        <a href="#">smartphones</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="#">smartphones</a>
            </li>
            <li>
                <a href="#">tv &amp; audio</a>
            </li>
            <li>
                <a href="#">gadgets</a>
            </li>
        </ul>
    </li>
    <li>
        <a href="#">car electronics</a>
        <div class="toggle"></div>
        <ul>
            <li>
                <a href="#">laptops &amp; computers</a>
                <div class="toggle"></div>
                <ul>
                    <li>
                        <a href="#">laptops &amp; computers</a>
                    </li>
                    <li>
                        <a href="#">video &amp; photo cameras</a>
                    </li>
                    <li>
                        <a href="#">smartphones</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="#">video &amp; photo cameras</a>
                <div class="toggle"></div>
                <ul>
                    <li>
                        <a href="#">video &amp; photo cameras</a>
                    </li>
                    <li>
                        <a href="#">smartphones</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="#">smartphones</a>
            </li>
            <li>
                <a href="#">tv &amp; audio</a>
            </li>
            <li>
                <a href="#">gadgets</a>
            </li>
        </ul>
    </li>
    <li>
        <a href="#">video games &amp; consoles</a>
    </li>
    <li>
        <a href="#">software</a>
    </li>
    <li>
        <a href="#">offeci accessories</a>
    </li>
    <li>
        <a href="#">accessories</a>
    </li>
</ul>
@extends('frontend.layout.app')

@section('content')
<div class="tab-entry visible">
    <div class="row nopadding">
        @foreach ($products as $product )
            <div class="col-sm-4">
                <div class="product-shortcode style-1">
                    <div class="title">
                        <div class="simple-article size-1 color col-xs-b5">
                            <a href="#">{{ $product->name }}</a>
                        </div>
                        <div class="h6 animate-to-green">
                            <a href="#">{{ $product->name }}</a>
                        </div>
                    </div>
                    <div class="preview">
                        @if($product->image)
                            <img src="{{ asset('/uploads/product/'.$product->image) }}" style="width: 225px;height: 225px;" alt=""/>        
                        @else
                            <img src="{{ asset('sb_admin/img/image.png') }}" style="width: 225px;height: 225px;" alt="no image">
                        @endif
                        <div class="preview-buttons valign-middle">
                            <div class="valign-middle-content">
                                <a class="button size-2 style-2" href="#">
                                    <span class="button-wrapper">
                                        <span class="icon"><img src="img/icon-1.png" alt=""></span>
                                        <span class="number">Learn More</span>
                                    </span>
                                </a>
                                <a class="button size-2 style-3" href="#">
                                    <span class="button-wrapper">
                                        <span class="icon"><img src="img/icon-3.png" alt=""></span>
                                        <span class="text">Add To Cart</span>
                                    </span>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="price">
                        <div class="simple-article size-4 dark">${{ $product->price }}</div>
                    </div>
                    <div class="description">
                        <div class="simple-article text size-2">{{ $product->description }}</div>
                        <div class="icons">
                            <a class="entry"><i class="bi bi-check-lg" aria-hidden="true"></i></a>
                            <a class="entry open-popup" data-rel="3">
                                <i class="bi bi-eye-fill" aria-hidden="true"></i>
                            </a>
                            <a class="entry"><i class="bi bi-heart" aria-hidden="true"></i></a>
                        </div>
                    </div>
                </div>  
            </div>
            
        @endforeach
    </div>
</div>
@endsection
@section('js')

@endsection
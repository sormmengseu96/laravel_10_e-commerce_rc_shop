@extends('admin.layout.app')

@section('content')
<div class="container-fluid">
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <div class="h5 mb-0 text-gray-800">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="">brand</a></li>
                    <li class="breadcrumb-item"><a href="#">view</a></li>
                </ol>
            </nav>
        </div>
        <a href="{{ route('brand.edit',$brand->id)}}" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm">
            <i class="fas fa-edit fa-sm text-white-50"></i> Edit
        </a>
    </div>
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">BRAND VIEW 
                <i class="fa fa-eye"></i>
            </h6>
        </div>
        <div class="card-body py-3">
            <div class="row g-3">
                <div class="col-md-5">
                    <label for="name">Name</label>
                    <input type="name" class="form-control  " name="name" id="name" placeholder="brand Name" value="{{ $brand->name }}">
                    <p></p>
                </div>
                <div class="col-md-5">
                    <label for="slug">slug</label>
                    <input type="slug" class="form-control  " name="slug" id="slug" placeholder="brand Name" value="{{ $brand->slug }}">
                    <p></p>
                </div>
                <div class="col-md-2">
                    <label for="status">Status</label>
                    <input class="form-control  " name="status" value="{{ $brand->status == 1 ? 'Active' : 'Block' }}">
                </div>
                <div class="col-md-12">
                    <label for="description">Description</label>
                    <textarea class="form-control  " name="description" id="floatingInput" placeholder="description" >
                        {{ $brand->description }}
                    </textarea>
                </div>
                <div class="col-12">
                    @if($brand->logo != Null)
                        <img src="{{ asset('/uploads/brands/'.$brand->logo) }}" class="img-thumbnail  " style="width:250px;height:250px; border-radius: 20px; " >
                    @else
                        <img src="{{ asset('sb_admin/img/image.png') }}" class="img-thumbnail  " style="width:250px;height:250px; border-radius: 20px; " >
                    @endif
                </div>
            </div>
        </div>
        <div class="card-footer py-3">
            <button onclick="window.location.href='{{ route('brands.index') }}'" class="btn btn-danger btn-sm">Back</button>
        </div>
    </div>
</div>
@endsection
@section('customjs')

@endsection

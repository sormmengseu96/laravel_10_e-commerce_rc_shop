@extends('admin.layout.app')

@section('content')
<div class="page-wrapper">
  <!-- Page header -->
  <div class="page-header d-print-none">
      <div class="container-xl">
          <div class="row g-2 align-items-center">
              @include('admin.message')
              <div class="col">
                  <h2 class="page-title">
                      Products list
                  </h2>
              </div>
              <div class="col-auto ms-auto d-print-none">
                  <div class="btn-list">
                      <form>
                          <div class="row g-2">
                              <div class="col">
                                  <input type="text" class="form-control" placeholder="Search for…"
                                      value="{{ Request::get('keyword') }}" name="keyword">
                              </div>
                              <div class="col-auto">
                                  <button type="submit" class="btn btn-icon" aria-label="Button">
                                      <!-- Download SVG icon from http://tabler-icons.io/i/search -->
                                      <svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24"
                                          height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor"
                                          fill="none" stroke-linecap="round" stroke-linejoin="round">
                                          <path stroke="none" d="M0 0h24v24H0z" fill="none" />
                                          <path d="M10 10m-7 0a7 7 0 1 0 14 0a7 7 0 1 0 -14 0" />
                                          <path d="M21 21l-6 -6" />
                                      </svg>
                                  </button>
                              </div>
                          </div>
                      </form>
                      <button class="btn btn-orange" onclick="print()">
                          <!-- Download SVG icon from http://tabler-icons.io/i/plus -->
                          <svg  xmlns="http://www.w3.org/2000/svg"  width="24"  height="24"  viewBox="0 0 24 24"  fill="none"  stroke="currentColor"  stroke-width="2"  stroke-linecap="round"  stroke-linejoin="round"  class="icon icon-tabler icons-tabler-outline icon-tabler-printer"><path stroke="none" d="M0 0h24v24H0z" fill="none"/><path d="M17 17h2a2 2 0 0 0 2 -2v-4a2 2 0 0 0 -2 -2h-14a2 2 0 0 0 -2 2v4a2 2 0 0 0 2 2h2" /><path d="M17 9v-4a2 2 0 0 0 -2 -2h-6a2 2 0 0 0 -2 2v4" /><path d="M7 13m0 2a2 2 0 0 1 2 -2h6a2 2 0 0 1 2 2v4a2 2 0 0 1 -2 2h-6a2 2 0 0 1 -2 -2z" /></svg>
                          Print
                      </button>
                      <button onclick="window.location.href='{{ route('products.create') }}'" class="btn btn-lime">
                          <!-- Download SVG icon from http://tabler-icons.io/i/plus -->
                          <svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24"
                              viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none"
                              stroke-linecap="round" stroke-linejoin="round">
                              <path stroke="none" d="M0 0h24v24H0z" fill="none" />
                              <path d="M12 5l0 14" />
                              <path d="M5 12l14 0" />
                          </svg>
                          Create new product
                      </button>
                      <a href="#" class="btn btn-primary d-sm-none btn-icon" data-bs-toggle="modal"
                          data-bs-target="#modal-report" aria-label="Create new report">
                          <!-- Download SVG icon from http://tabler-icons.io/i/plus -->
                          <svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24"
                              viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none"
                              stroke-linecap="round" stroke-linejoin="round">
                              <path stroke="none" d="M0 0h24v24H0z" fill="none" />
                              <path d="M12 5l0 14" />
                              <path d="M5 12l14 0" />
                          </svg>
                      </a>
                  </div>
              </div>
          </div>
      </div>
  </div>
  <!-- Page body -->
  <div class="page-body">
      <div class="container-xl">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                  <table class="table table-vcenter">
                    <thead>
                      <tr>
                        <th>#ID</th>
                        <th>Image</th>
                        <th>Title</th>
                        <th>Slug</th>
                        <!-- <th>Description</th> -->
                        <th>Weight</th>
                        <th>stock</th>
                        <th width="100">price</th>
                        <th>status</th>
                        <th>Show in home</th>
                        <th>Category</th>
                        <th>Brand</th>
                        <th>Create</th>
                        <th class="w-1"></th>
                      </tr>
                    </thead>
                    <tbody>
                        @if($products->IsNotEmpty())
                            @foreach ($products as $product )
                            <tr>
                                <td>#{{ $product->id }}</td>
                                <td data-label="Name" >
                                <div class="d-flex py-1 align-items-center">
                                    {{-- <span class="avatar me-2" style="background-image: url(./static/avatars/010m.jpg)"></span> --}}
                                    <img src="{{ asset('/uploads/product/'.$product->image) }}" class="avatar me-2" />
                                </div>
                                </td>
                                <td data-label="Title" >
                                {{ $product->title }}
                                </td>
                                <td class="text-secondary" data-label="Role" >
                                {{ $product->slug }}
                                </td>
                                <!-- <td>
                                    {{ $product->description }}
                                </td> -->
                                <td>
                                    {{ $product->weight }} kg
                                </td>
                                <td>
                                    <span class="{{ $product->in_stock == 1 ? 'badge badge-outline text-green' : 'badge badge-outline text-red'  }}">
                                        {{ $product->in_stock == 1 ? 'in stock' : 'out stock'}}
                                    </span>
                                </td>
                                <td>${{ $product->price }}</td>
                                <td>
                                    <span class="{{ $product->status == 1 ? 'badge badge-outline text-green' : 'badge badge-outline text-red'  }}">
                                        {{ $product->status == 1 ? 'Active' : 'Block'}}
                                    </span>
                                </td>
                                <td>
                                    <span class="{{ $product->show_in_home == 'Yes' ? 'badge badge-outline text-green' : 'badge badge-outline text-red' }}">
                                        {{ $product->show_in_home == 'Yes' ? 'Yes' : 'No' }}
                                    </span>
                                </td>
                                <td>#{{ $product->categoryID }}</td>
                                <td>#{{ $product->brandID}}</td>
                                <td>{{ $product->created_at->format('d/m/y') }}</td>
                                <td>
                                    <div class="btn-list flex-nowrap">
                                        <div class="col-6 col-sm-2 col-md-2 col-xl py-3">
                                            <a href="{{ route('product.edit',$product->id) }}" class="btn btn-orange btn-square btn-sm">
                                                <svg  xmlns="http://www.w3.org/2000/svg"  width="30"  height="30"  viewBox="0 0 24 24"  fill="none"  stroke="currentColor"  stroke-width="2"  stroke-linecap="round"  stroke-linejoin="round"  class="icon icon-tabler icons-tabler-outline icon-tabler-edit"><path stroke="none" d="M0 0h24v24H0z" fill="none"/><path d="M7 7h-1a2 2 0 0 0 -2 2v9a2 2 0 0 0 2 2h9a2 2 0 0 0 2 -2v-1" /><path d="M20.385 6.585a2.1 2.1 0 0 0 -2.97 -2.97l-8.415 8.385v3h3l8.385 -8.415z" /><path d="M16 5l3 3" /></svg>
                                                Update
                                            </a>
                                        </div>
                                        <div class="col-6 col-sm-2 col-md-2 col-xl py-3">
                                            <button class="btn btn-danger btn-square btn-sm" data-bs-toggle="modal" data-bs-target="#modal-delete" onclick="deleteID({{ $product->id }})" >
                                                <svg  xmlns="http://www.w3.org/2000/svg"  width="24"  height="24"  viewBox="0 0 24 24"  fill="none"  stroke="currentColor"  stroke-width="2"  stroke-linecap="round"  stroke-linejoin="round"  class="icon icon-tabler icons-tabler-outline icon-tabler-trash-x"><path stroke="none" d="M0 0h24v24H0z" fill="none"/><path d="M4 7h16" /><path d="M5 7l1 12a2 2 0 0 0 2 2h8a2 2 0 0 0 2 -2l1 -12" /><path d="M9 7v-3a1 1 0 0 1 1 -1h4a1 1 0 0 1 1 1v3" /><path d="M10 12l4 4m0 -4l-4 4" /></svg>
                                            Delete
                                            </button>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="13"></td>
                            </tr>
                        @endif
                    </tbody>
                  </table>
                </div>
              </div>
        </div>
      </div>
  </div>

  <footer class="footer footer-transparent d-print-none">
      <div class="container-xl">
          <div class="row text-center align-items-center flex-row-reverse">
              <div class="col-lg-auto ms-lg-auto">
                  <ul class="list-inline list-inline-dots mb-0">
                      <li class="list-inline-item"><a href="https://tabler.io/docs" target="_blank"
                              class="link-secondary" rel="noopener">Documentation</a></li>
                      <li class="list-inline-item"><a href="./license.html" class="link-secondary">License</a></li>
                      <li class="list-inline-item"><a href="https://github.com/tabler/tabler" target="_blank"
                              class="link-secondary" rel="noopener">Source code</a></li>
                      <li class="list-inline-item">
                          <a href="https://github.com/sponsors/codecalm" target="_blank" class="link-secondary"
                              rel="noopener">
                              <!-- Download SVG icon from http://tabler-icons.io/i/heart -->
                              <svg xmlns="http://www.w3.org/2000/svg" class="icon text-pink icon-filled icon-inline"
                                  width="24" height="24" viewBox="0 0 24 24" stroke-width="2"
                                  stroke="currentColor" fill="none" stroke-linecap="round"
                                  stroke-linejoin="round">
                                  <path stroke="none" d="M0 0h24v24H0z" fill="none" />
                                  <path
                                      d="M19.5 12.572l-7.5 7.428l-7.5 -7.428a5 5 0 1 1 7.5 -6.566a5 5 0 1 1 7.5 6.572" />
                              </svg>
                              Sponsor
                          </a>
                      </li>
                  </ul>
              </div>
              <div class="col-12 col-lg-auto mt-3 mt-lg-0">
                  <ul class="list-inline list-inline-dots mb-0">
                      <li class="list-inline-item">
                          Copyright &copy; 2023
                          <a href="." class="link-secondary">Tabler</a>.
                          All rights reserved.
                      </li>
                      <li class="list-inline-item">
                          <a href="./changelog.html" class="link-secondary" rel="noopener">
                              v1.0.0-beta20
                          </a>
                      </li>
                  </ul>
              </div>
          </div>
      </div>
  </footer>

  {{-- modal --}}
  <div class="modal modal-blur fade" id="modal-delete" tabindex="-1" role="dialog" aria-hidden="true" > 
    <div class="modal-dialog modal-sm modal-dialog-centered" role="document">
      <div class="modal-content">
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        <div class="modal-status bg-danger"></div>
        <div class="modal-body text-center py-4">
          <!-- Download SVG icon from http://tabler-icons.io/i/alert-triangle -->
          <svg xmlns="http://www.w3.org/2000/svg" class="icon mb-2 text-danger icon-lg" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"><path stroke="none" d="M0 0h24v24H0z" fill="none"/><path d="M10.24 3.957l-8.422 14.06a1.989 1.989 0 0 0 1.7 2.983h16.845a1.989 1.989 0 0 0 1.7 -2.983l-8.423 -14.06a1.989 1.989 0 0 0 -3.4 0z" /><path d="M12 9v4" /><path d="M12 17h.01" /></svg>
          <h3>Are you sure?</h3>
          <div class="text-secondary">Do you really want to remove <span id="DeleteProducTitle">85</span>? What you've done cannot be undone.</div>
        </div>
        <div class="modal-footer">
          <div class="w-100">
            <div class="row">
              <div class="col"><a href="#" class="btn w-100" data-bs-dismiss="modal">
                  Cancel
                </a></div>
              <div class="col"><button onclick="deleteProduct()" class="btn btn-danger w-100" >
                  Delete <span id="show_id"></span>
                  {{-- <input type="text" id="id_show"> --}}
              </button></div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

</div>
@endsection

@section('customjs')
<script>
    function deleteID(id){
        // alert()
        $('#show_id').html(" " + "#" +id);
        $('#DeleteProducTitle').html("#" + id);
        
        var del = $("#modal-delete").val(id);
    }
    function deleteProduct() {
        var id = $('#modal-delete').val();

        $.ajax({
            type: "get",
            url: "/admin/product/" + id + "/delete" ,
            data: "data",
            dataType: "json",
            success: function (response) {
                if(response['status'] == true){
                    window.location.href = "{{ route('products.index') }}";
                }
            }
        });
    }
</script>
@endsection

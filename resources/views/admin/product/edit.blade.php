@extends('admin.layout.app')

@section('content')
{{-- <link href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css" rel="stylesheet"> --}}
<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css" rel="stylesheet">
<script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.js"></script>
    <div class="page-wrapper">
        <!-- Page header -->
        <div class="page-header d-print-none">
            <div class="container-xl">
                <div class="row g-2 align-items-center">
                    <div class="col">
                        <!-- Page pre-title -->
                        <div class="page-pretitle">
                            Overview
                        </div>
                        <h2 class="page-title">
                            Dashboard
                        </h2>
                    </div>
                    <!-- Page title actions -->
                    <div class="col-auto ms-auto d-print-none">
                        <div class="btn-list">
                            <a href="{{ route('products.index') }}" class="btn btn-orange d-none d-sm-inline-block">
                                <svg  xmlns="http://www.w3.org/2000/svg"  width="24"  height="24"  viewBox="0 0 24 24"  fill="none"  stroke="currentColor"  stroke-width="2"  stroke-linecap="round"  stroke-linejoin="round"  class="icon icon-tabler icons-tabler-outline icon-tabler-arrow-left"><path stroke="none" d="M0 0h24v24H0z" fill="none"/><path d="M5 12l14 0" /><path d="M5 12l6 6" /><path d="M5 12l6 -6" /></svg>
                                Back
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Page body -->
        <div class="page-body">
            <div class="container-xl">
                    <div class="col-lg-12">
                        <div class="row row-cards">
                            <div class="col-12">
                                {{-- form submit product  --}}
                                <form class="card" id="productFormUpdate" name="productFormUpdate" data-id="{{ $product->id }}">
                                    <div class="card-body">
                                        <h3 class="card-title">Add New Product</h3>
                                        <div class="row row-cards">
                                            <div class="col-md-6">
                                                <div class="mb-3">
                                                    <label class="form-label required">Title of product</label>
                                                    <input type="text" class="form-control" placeholder="Title of product" name="title" id="title" value="{{ $product->title }}">
                                                    <p></p>
                                                </div>  
                                            </div>
                                            <div class="col-md-6">
                                                <div class="mb-3">
                                                    <label class="form-label required">Slug</label>
                                                    <input type="text" class="form-control" placeholder="Slug" name="slug" id="slug" value="{{ $product->slug }}">
                                                    <p></p>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="mb-3">
                                                    <label class="form-label required">Category</label>
                                                    <select class="form-control form-select" name="category" id="category">
                                                        @if($categories->IsNotEmpty())
                                                            @foreach ($categories as $category )
                                                                <option value="{{ $category->id }}">{{$category->id . " - ". $category->name }}</option>
                                                            @endforeach
                                                        @else
                                                            <option value="0">Categories not found</option>
                                                        @endif
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="mb-3">
                                                    <label class="form-label required">Brand</label>
                                                    <select class="form-control form-select" name="brand" id="brand">
                                                        @if($brands->IsNotEmpty())
                                                            @foreach ($brands as $brand )
                                                                <option value="{{ $brand->id }}">{{$brand->id . " - " .$brand->name }}</option>
                                                            @endforeach
                                                        @else
                                                            <option value="0">Brands not found</option>
                                                        @endif
                                                    </select>
                                                </div>
                                            </div>
                             
                                            <div class="col-md-12">
                                                <div class="mb-3">
                                                    <label class="form-label">Description</label>
                                                    <textarea id="summernote" name="description">{{ $product->description }}</textarea>

                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="mb-3">
                                                    <label class="form-label required">Weight</label>
                                                    <div class="input-icon mb-3">
                                                         <input type="text" class="form-control" placeholder="Weight of product" name="weight" id="weight" value="{{ $product->weight }}">
                                                        <p></p>
                                                      </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="mb-3">
                                                    <label class="form-label required">Price</label>
                                                    <div class="input-icon mb-3">
                                                        <input type="text" class="form-control" placeholder="Price" name="price" id="price" value="{{ $product->price }}">
                                                        <p></p>
                                                      </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="mb-3">
                                                    <label class="form-label required">SKU</label>
                                                    <input type="text" class="form-control" placeholder="Stock keeping unit" name="sku" id="sku" value="{{ $product->SKU }}">
                                                    <p></p>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="mb-3">
                                                    <label class="form-label required">Status</label>
                                                    <select class="form-select" name="status" id="status">
                                                        <option {{ $product->status ==  1 ? 'Selected' : '' }} value="1">Active</option>
                                                        <option {{ $product->status ==  0 ? 'Selected' : '' }} value="0">Block</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="mb-3">
                                                    <label class="form-label required">Stock</label>
                                                    <select class="form-select" name="stock" id="stock">
                                                        <option {{ $product->stock == 1 ? 'Selected' : '' }} value="1">In stock</option>
                                                        <option {{ $product->stock == 0 ? 'Selected' : '' }} value="0">Out stock</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="mb-3">
                                                    <div class="form-label">Show to home</div>
                                                    <label class="form-check form-switch">
                                                      <input class="form-check-input" type="checkbox" name="show_in_home" {{ $product->show_in_home == 'Yes' ? 'checked' : '' }}>
                                                      <span class="form-check-label">This will appear on home page</span>
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <h3 class="card-title">Upload<input type="text" id="image_id" name="image_id" hidden/></h3>
                                                <div class="dropzone" id="dropzone-default" action="./"
                                                    autocomplete="off" novalidate
                                                    style="border: 1px dashed rgb(195, 190, 190)">
                                                    <div class="fallback">
                                                        <input name="file" type="file" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-footer text-end">
                                        <button type="submit" class="btn btn-success">Update</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <footer class="footer footer-transparent d-print-none">
            <div class="container-xl">
                <div class="row text-center align-items-center flex-row-reverse">
                    <div class="col-lg-auto ms-lg-auto">
                        <ul class="list-inline list-inline-dots mb-0">
                            <li class="list-inline-item"><a href="https://tabler.io/docs" target="_blank"
                                    class="link-secondary" rel="noopener">Documentation</a></li>
                            <li class="list-inline-item"><a href="./license.html" class="link-secondary">License</a>
                            </li>
                            <li class="list-inline-item"><a href="https://github.com/tabler/tabler" target="_blank"
                                    class="link-secondary" rel="noopener">Source code</a></li>
                            <li class="list-inline-item">
                                <a href="https://github.com/sponsors/codecalm" target="_blank" class="link-secondary"
                                    rel="noopener">
                                    <!-- Download SVG icon from http://tabler-icons.io/i/heart -->
                                    <svg xmlns="http://www.w3.org/2000/svg" class="icon text-pink icon-filled icon-inline"
                                        width="24" height="24" viewBox="0 0 24 24" stroke-width="2"
                                        stroke="currentColor" fill="none" stroke-linecap="round"
                                        stroke-linejoin="round">
                                        <path stroke="none" d="M0 0h24v24H0z" fill="none" />
                                        <path
                                            d="M19.5 12.572l-7.5 7.428l-7.5 -7.428a5 5 0 1 1 7.5 -6.566a5 5 0 1 1 7.5 6.572" />
                                    </svg>
                                    Sponsor
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-12 col-lg-auto mt-3 mt-lg-0">
                        <ul class="list-inline list-inline-dots mb-0">
                            <li class="list-inline-item">
                                Copyright &copy; 2023
                                <a href="." class="link-secondary">Tabler</a>.
                                All rights reserved.
                            </li>
                            <li class="list-inline-item">
                                <a href="./changelog.html" class="link-secondary" rel="noopener">
                                    v1.0.0-beta20
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </footer>
    </div>
@endsection
@section('customjs')

<script>
    document.addEventListener("DOMContentLoaded", function() {

        Dropzone.autoDiscover = false;
            const dropzone = $("#dropzone-default").dropzone({
                init: function ( ) {
                    this.on('addefile',function (file) {
                        if(this.files.length > 1 ){
                            this.removeFile(this.files[0]);
                        }
                    });
                },    
                url: "{{ route('image.create') }}",
                maxFiles : 1,
                paramName : 'image',
                addRemoveLinks:true,
                acceptedFiles:"image/jpeg,image/png,image/gif",
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                },success:function (file,response) {
                    var get_image_id = response['image_id'];
                    $("#image_id").val(get_image_id)

                    // console.log(get_image_id);
                }
            })
        });
</script>
<script>
    $(document).ready(function() {
            $('#summernote').summernote({
                placeholder: 'Hello stand alone ui',
                tabsize: 2,
                height: 120,
                toolbar: [
                // ['style', ['style']],
                ['font', ['bold', 'underline',]],
                // ['color', ['color']],
                ['para', ['ul', 'ol',]],
                // ['table', ['table']],
                // ['insert', ['link', 'picture', 'video']],
                ['view', ['codeview']]
                ]

            });
        });
</script>
    <script>
        $( "#productFormUpdate" ).on( "submit", function( event ) {
            var id  = $(this).data('id');
            event.preventDefault();
            var element = $(this);
            $.ajax({
                type: "POST",
                url: "/admin/product/" + id + "/update",
                data: element.serializeArray(),
                dataType: "json",
                success: function (response) {
                    var error = response['errors'];
                    if(response['status'] == true){
                        window.location.href = '{{ route("products.index") }}'
                    }

                    if(error['title']){
                            $('#title').addClass('is-invalid').siblings('p').addClass('invalid-feedback').html(error['title']);
                        } 
                        if(error['slug']){
                            $('#slug').addClass('is-invalid').siblings('p').addClass('invalid-feedback').html(error['slug']);
                        }
                        if(error['category']){
                            $('#category').addClass('is-invalid').siblings('p').addClass('invalid-feedback').html(error['category']);
                        }
                        if(error['brand']){
                            $('#brand').addClass('is-invalid').siblings('p').addClass('invalid-feedback').html(error['brand']);
                        }
                        if(error['weight']){
                            $('#weight').addClass('is-invalid').siblings('p').addClass('invalid-feedback').html(error['weight']);
                        }
                        if(error['price']){
                            $('#price').addClass('is-invalid').siblings('p').addClass('invalid-feedback').html(error['price']);
                        }
                        if(error['sku']){
                            $('#sku').addClass('is-invalid').siblings('p').addClass('invalid-feedback').html(error['sku']);
                        }
                        $(document).ready(function () {
                            $('#title').on('input', function () {
                                $('#title').removeClass('is-invalid');
                            });
                            $('#weight').on('input',function () {  
                                $('#weight').removeClass('is-invalid')
                            })
                            $('#price').on('input',function () {  
                                $('#price').removeClass('is-invalid')
                            })
                            $('#sku').on('input',function () {  
                                $('#sku').removeClass('is-invalid')
                            })
                        });
                   
                    // console.log(errors);
                },error: function(jqXHR, exception) {
                    console.log('Something went wrong');
                }
            });
        });
        $("#title").change(function() {
            element = $(this);
            $.ajax({
                type: "get",
                url: "{{ route('getSlug') }}",
                data: {
                    title: element.val()
                },
                dataType: "json",
                success: function(response) {
                    if (response["status"] == true) {
                        $("#slug").val(response["slug"]).removeClass('is-invalid');
                    }
                }
            });
        });
    </script>
@endsection
@php
// dd($products->compate);
@endphp
@extends('admin.layout.app')

@section('content')
<div class="container-fluid">
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <div class="h5 mb-0 text-gray-800">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="">product</a></li>
                  <li class="breadcrumb-item"><a href="#">show</a></li>
                </ol>
            </nav>  
        </div>
    </div>
    <div class="card shadow mb-4">
        <form method="POST" name="productForm" id="productForm">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary">PRODUCT SHOW 
                    <i class="fa fa-edit"></i>
                </h6>
            </div>
            <div class="card-body py-3">
                <div class="row g-3">
                    <div class="col-md-5">
                        <label for="name">Name</label>
                        <input type="name" class="form-control" name="name" id="name" placeholder="product name" value="{{ $products->name }}">
                        <p></p>
                    </div>
                    <div class="col-md-5">
                        <label for="slug">Slug</label>
                        <input type="slug" class="form-control" name="slug" id="slug" placeholder="product slug" value="{{ $products->slug }}">
                        <p></p>
                    </div>
                    <div class="col-md-2">
                        <label for="slug">Status</label>
                        <input class="form-control" name="status" id="status" value="{{ $products->status = 1 ? 'Active' : 'Block' }}">
                    </div>
                    <div class="col-md-12">
                        <label for="description">Description</label>
                        <textarea class="summernote" name="description" id="description" >{{ $products->description }}</textarea>
                    </div>
                    <div class="col-md-6">
                        <label for="category">Category ID</label>
                        <input type="text" class="form-control" value="{{ $categories->name}}">
                    </div>
                    <div class="col-md-3">
                        <label for="price">Price</label>
                        <input type="number" class="form-control" name="price" id="price" placeholder="price" value="{{ $products->price }}">
                        <p></p>
                    </div>
                    <div class="col-md-3">
                        <label for="barcode">Weight</label>
                        <input type="number" class="form-control" name="weight_of_product" id="weight_of_product" placeholder="weight of product" value="{{ $products->weight}}">
                        <p></p>
                    </div>
                    <div class="col-md-6">
                        <label for="brand">Brand ID</label>
                        <input type="text" class="form-control" value="{{ $products->name}}">
                    </div>
                    <div class="col-md-3">
                        <label for="barcode">Barcode</label>
                        <input type="text" class="form-control  " name="barcode" id="barcode" placeholder="barcode" value="{{ $products->barcode }}">
                        <p></p>
                    </div>
                    <div class="col-md-6">
                        <label for="slug">In-stock</label>
                        <input type="number" class="form-control" name="instock" id="instock" placeholder="instock" value="{{ $products->stock }}">
                        <p></p>
                    </div>
                    <div class="col-md-6">
                        <label for="sku">Stock keeping unit(sku)</label>
                        <input type="number" class="form-control " name="stock_keeping_unit" id="stock_keeping_unit" placeholder="stock keeping unit" value="{{ $products->sku }}">
                        <p></p>
                    </div>
                    <div class="col-12">
                        @if($products->image != Null)
                            <img src="{{ asset('/uploads/product/'.$products->image) }}" class="img-thumbnail border-dark" style="width:250px;height:250px; border-radius: 20px; " >
                        @else
                            <img src="{{ asset('sb_admin/img/image.png') }}" class="img-thumbnail border-dark" style="width:250px;height:250px; border-radius: 20px; " >
                        @endif
                    </div>
                  
                </div>
            </div>
            <div class="card-footer py-3">
                <a class="btn btn-outline-dark btn-sm" onclick="window.location.href='{{ route('products.index') }}'">Cancel</a>
            </div>
        </form>
    </div>
</div>
@endsection

@section('customjs')
<script>
$(document).ready(function() {

// $('.summernote').summernote();
$('#description').summernote({
    placeholder: 'description',
    tabsize:2,
    height:300
})

});
</script>
@endsection
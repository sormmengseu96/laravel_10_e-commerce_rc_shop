@php
    // dd($categories)
@endphp
@extends('admin.layout.app')

@section('content')
    <div class="page-wrapper">
        <!-- Page header -->
        <div class="page-header d-print-none">
            <div class="container-xl">
                <div class="row g-2 align-items-center">
                    @include('admin.message')
                    <div class="col">
                        <h2 class="page-title">
                            Categorie list
                        </h2>
                    </div>
                    <div class="col-auto ms-auto d-print-none">
                        <div class="btn-list">
                            <form>
                                <div class="row g-2">
                                    <div class="col">
                                        <input type="text" class="form-control" placeholder="Search for…"
                                            value="{{ Request::get('keyword') }}" name="keyword">
                                    </div>
                                    <div class="col-auto">
                                        <button type="submit" class="btn btn-icon" aria-label="Button">
                                            <!-- Download SVG icon from http://tabler-icons.io/i/search -->
                                            <svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24"
                                                height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor"
                                                fill="none" stroke-linecap="round" stroke-linejoin="round">
                                                <path stroke="none" d="M0 0h24v24H0z" fill="none" />
                                                <path d="M10 10m-7 0a7 7 0 1 0 14 0a7 7 0 1 0 -14 0" />
                                                <path d="M21 21l-6 -6" />
                                            </svg>
                                        </button>
                                    </div>
                                </div>
                            </form>
                            <button class="btn btn-orange" onclick="print()">
                                <!-- Download SVG icon from http://tabler-icons.io/i/plus -->
                                <svg  xmlns="http://www.w3.org/2000/svg"  width="24"  height="24"  viewBox="0 0 24 24"  fill="none"  stroke="currentColor"  stroke-width="2"  stroke-linecap="round"  stroke-linejoin="round"  class="icon icon-tabler icons-tabler-outline icon-tabler-printer"><path stroke="none" d="M0 0h24v24H0z" fill="none"/><path d="M17 17h2a2 2 0 0 0 2 -2v-4a2 2 0 0 0 -2 -2h-14a2 2 0 0 0 -2 2v4a2 2 0 0 0 2 2h2" /><path d="M17 9v-4a2 2 0 0 0 -2 -2h-6a2 2 0 0 0 -2 2v4" /><path d="M7 13m0 2a2 2 0 0 1 2 -2h6a2 2 0 0 1 2 2v4a2 2 0 0 1 -2 2h-6a2 2 0 0 1 -2 -2z" /></svg>
                                Print
                            </button>
                            <button href="{{ route('categories.create') }}" class="btn btn-lime" data-bs-toggle="modal" data-bs-target="#modal-large">
                                <!-- Download SVG icon from http://tabler-icons.io/i/plus -->
                                <svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24"
                                    viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none"
                                    stroke-linecap="round" stroke-linejoin="round">
                                    <path stroke="none" d="M0 0h24v24H0z" fill="none" />
                                    <path d="M12 5l0 14" />
                                    <path d="M5 12l14 0" />
                                </svg>
                                Create new category
                            </button>
                            <a href="#" class="btn btn-primary d-sm-none btn-icon" data-bs-toggle="modal"
                                data-bs-target="#modal-report" aria-label="Create new report">
                                <!-- Download SVG icon from http://tabler-icons.io/i/plus -->
                                <svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24"
                                    viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none"
                                    stroke-linecap="round" stroke-linejoin="round">
                                    <path stroke="none" d="M0 0h24v24H0z" fill="none" />
                                    <path d="M12 5l0 14" />
                                    <path d="M5 12l14 0" />
                                </svg>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Page body -->
        <div class="page-body">
            <div class="container-xl">
                <div class="card">
                    <div class="card-body">
                        <div id="table-default" class="table-responsive">
                            <table class="table table-vcenter">
                                <thead>
                                    <tr>
                                        <th>#ID</th>
                                        <th>Image</th>
                                        <th>Name</th>
                                        <th>Slug</th>
                                        <th>Description</th>
                                        <th>Status</th>
                                        <th>Created Date</th>
                                        <th>show on home</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($categories as $category)
                                        <tr>
                                            <td>{{ $category->id }}</td>
                                            <td>
                                                <div class="row g-3 align-items-center">
                                                    <a href="#" class="col-auto">
                                                        @if($category->image)
                                                            <img src="{{ asset('/uploads/category/'.$category->image) }}" class="avatar">
                                                        @else
                                                            <img src="{{ asset('/no_image/No-Image-Placeholder.png') }}" class="avatar">
                                                        @endif
                                                    </a>
                                                    <div class="col text-truncate">
                                                    </div>
                                                </div>
                                            </td>
                                            <td>{{ $category->name }}</td>
                                            <td>{{ $category->slug }}</td>
                                            <td>{{ $category->description }}</td>
                                            <td>
                                                <span class="badge bg-orange text-orange-fg">
                                                    {{ $category->status == 1 ? 'Active' : 'Block' }}
                                                </span>
                                            </td>
                                            <td>{{ $category->created_at->format('D/m/Y') }}</td>
                                            <td>
                                                <label class="form-check form-switch">
                                                    <input class="form-check-input bg-lime" type="checkbox" {{ $category->show_to_home == "Yes" ? "checked" : "" }} disabled>
                                                </label>
                                            </td>
                                            <td>
                                                <div class="btn-list flex-nowrap">
                                                    <div class="col-6 col-sm-2 col-md-2 col-xl py-3">
                                                        <button id="categoryUpdate" data-bs-toggle="modal" data-bs-target="#modal-update" onclick="categoryUpdate('{{ $category->id }}','{{ $category->name }}','{{ $category->slug }}','{{ $category->status }}','{{ $category->show_to_home }}','{{ $category->description}}','{{ $category->image }}')" class="btn btn-orange btn-square btn-sm">
                                                            <svg  xmlns="http://www.w3.org/2000/svg"  width="30"  height="30"  viewBox="0 0 24 24"  fill="none"  stroke="currentColor"  stroke-width="2"  stroke-linecap="round"  stroke-linejoin="round"  class="icon icon-tabler icons-tabler-outline icon-tabler-edit"><path stroke="none" d="M0 0h24v24H0z" fill="none"/><path d="M7 7h-1a2 2 0 0 0 -2 2v9a2 2 0 0 0 2 2h9a2 2 0 0 0 2 -2v-1" /><path d="M20.385 6.585a2.1 2.1 0 0 0 -2.97 -2.97l-8.415 8.385v3h3l8.385 -8.415z" /><path d="M16 5l3 3" /></svg>
                                                            Update
                                                        </button>
                                                    </div>
                                                    <div class="col-6 col-sm-2 col-md-2 col-xl py-3">
                                                        <button class="btn btn-danger btn-square btn-sm" data-bs-toggle="modal" data-bs-target="#modal-delete" onclick="get_id({{ $category->id }})">
                                                            <svg  xmlns="http://www.w3.org/2000/svg"  width="24"  height="24"  viewBox="0 0 24 24"  fill="none"  stroke="currentColor"  stroke-width="2"  stroke-linecap="round"  stroke-linejoin="round"  class="icon icon-tabler icons-tabler-outline icon-tabler-trash-x"><path stroke="none" d="M0 0h24v24H0z" fill="none"/><path d="M4 7h16" /><path d="M5 7l1 12a2 2 0 0 0 2 2h8a2 2 0 0 0 2 -2l1 -12" /><path d="M9 7v-3a1 1 0 0 1 1 -1h4a1 1 0 0 1 1 1v3" /><path d="M10 12l4 4m0 -4l-4 4" /></svg>
                                                          Delete
                                                        </button>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
 
        <footer class="footer footer-transparent d-print-none">
            <div class="container-xl">
                <div class="row text-center align-items-center flex-row-reverse">
                    <div class="col-lg-auto ms-lg-auto">
                        <ul class="list-inline list-inline-dots mb-0">
                            <li class="list-inline-item"><a href="https://tabler.io/docs" target="_blank"
                                    class="link-secondary" rel="noopener">Documentation</a></li>
                            <li class="list-inline-item"><a href="./license.html" class="link-secondary">License</a></li>
                            <li class="list-inline-item"><a href="https://github.com/tabler/tabler" target="_blank"
                                    class="link-secondary" rel="noopener">Source code</a></li>
                            <li class="list-inline-item">
                                <a href="https://github.com/sponsors/codecalm" target="_blank" class="link-secondary"
                                    rel="noopener">
                                    <!-- Download SVG icon from http://tabler-icons.io/i/heart -->
                                    <svg xmlns="http://www.w3.org/2000/svg" class="icon text-pink icon-filled icon-inline"
                                        width="24" height="24" viewBox="0 0 24 24" stroke-width="2"
                                        stroke="currentColor" fill="none" stroke-linecap="round"
                                        stroke-linejoin="round">
                                        <path stroke="none" d="M0 0h24v24H0z" fill="none" />
                                        <path
                                            d="M19.5 12.572l-7.5 7.428l-7.5 -7.428a5 5 0 1 1 7.5 -6.566a5 5 0 1 1 7.5 6.572" />
                                    </svg>
                                    Sponsor
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-12 col-lg-auto mt-3 mt-lg-0">
                        <ul class="list-inline list-inline-dots mb-0">
                            <li class="list-inline-item">
                                Copyright &copy; 2023
                                <a href="." class="link-secondary">Tabler</a>.
                                All rights reserved.
                            </li>
                            <li class="list-inline-item">
                                <a href="./changelog.html" class="link-secondary" rel="noopener">
                                    v1.0.0-beta20
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </footer>

        {{-- model create --}}
        <div class="modal modal-blur fade" id="modal-large" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
                <div class="modal-content">
                    <form name="categoryForm" id="categoryForm">
                        <div class="modal-header">
                            <h5 class="modal-title">Category Create</h5>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="modal-body">
                            <div class="mb-2">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label>Name</label>
                                        <input type="text" class="form-control" name="name" id="name" placeholder="category name">
                                        <p></p>
                                    </div>
                                    <div class="col-md-6">
                                        <label>Slug</label>
                                        <input type="text" class="form-control" name="slug" id="slug" placeholder="slug">
                                        <p></p>
                                    </div>
                                </div>
                            </div>
                            <div class="mb-2">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label>Status</label>
                                        <select class="form-select" name="status">
                                            <option value="1">Active</option>
                                            <option value="0">Block</option>
                                        </select>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-label">show to home</div>
                                        <label class="form-check form-switch">
                                          <input class="form-check-input bg-lime" type="checkbox" name="show_to_home">
                                          <span class="form-check-label">This will appear on home page</span>
                                        </label>
                                    </div>  
                                </div>
                            </div>
                            <div class="mb-2">
                                <div class="row">
                                    <div class="col-md-12">
                                        <label class="form-label">Description</label>
                                        <textarea class="form-control" rows="3" name="description"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="col-md-12">
                                <label class="form-label">Upload
                                    <input type="text" id="image_id" name="image_id" hidden>
                                </label>
                                <div class="dropzone" id="dropzone-default" action="./"  autocomplete="off" novalidate  style="border: 1px dashed rgb(195, 190, 190)">
                                    <div class="fallback">
                                        <input name="file_upload" type="file"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn me-auto" data-bs-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-lime">Create</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        {{-- model update --}}
        <div class="modal modal-blur fade" id="modal-update" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
                <div class="modal-content">
                    <form name="categoryForm_update" id="categoryForm_update">
                        <div class="modal-header">
                            <h5 class="modal-title">Large Update</h5>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="modal-body">
                            <div class="mb-2">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label>Name</label>
                                        <input type="text" class="form-control" name="name" id="categoryNameUpdate" placeholder="category name">
                                        <p></p>
                                    </div>
                                    <div class="col-md-6">
                                        <label>Slug</label>
                                        <input type="text" class="form-control" name="slug" id="categorySlugUpdate" placeholder="slug">
                                        <p></p>
                                    </div>
                                </div>
                            </div>
                            <div class="mb-2">
                                <div class="row">
                                    <div class="col-md-6">
                                        <label>Status</label>
                                        <select class="form-select" name="status" id="categoryUpdateStauts">
                                            <option value="1">Active</option>
                                            <option value="0">Block</option>
                                        </select>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-label">show to home</div>
                                        <label class="form-check form-switch">
                                          <input class="form-check-input bg-lime" type="checkbox" id="CategoryUpdateShowToHome" name="show_to_home">
                                          <span class="form-check-label">This will appear on home page</span>
                                        </label>
                                    </div>  
                                </div>
                            </div>
                            <div class="mb-2">
                                <div class="row">
                                    <div class="col-md-12">
                                        <label class="form-label">Description</label>
                                        <textarea class="form-control" rows="3" id="descriptionUpdate" name="description"></textarea>
                                        {{-- <input class="form-control" rows="3" id="descriptionUpdate"> --}}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-9">
                                    <label class="form-label">Upload
                                        <input type="text" id="image_id_update" name="image_id" hidden>
                                    </label>
                                    <div class="dropzone" id="dropzone-default-update" action="./"  autocomplete="off" novalidate  style="border: 1px dashed rgb(195, 190, 190)">
                                        <div class="fallback">
                                            <input name="file_upload" type="file"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <label class="form-label">Image
                                    </label>
                                    <div class="dropzone" id="dropzone-default-update" action="./"  autocomplete="off" novalidate  style="border: 1px dashed rgb(195, 190, 190)">
                                        <div class="fallback">
                                            <img src="#" id="categoryImage" width="110" height="110">
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn me-auto" data-bs-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-lime">Update</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        {{-- model message delete --}}
        <div class="modal modal-blur fade" id="modal-delete" tabindex="-1" role="dialog" aria-hidden="true" > 
            <div class="modal-dialog modal-sm modal-dialog-centered" role="document">
              <div class="modal-content">
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                <div class="modal-status bg-danger"></div>
                <div class="modal-body text-center py-4">
                  <!-- Download SVG icon from http://tabler-icons.io/i/alert-triangle -->
                  <svg xmlns="http://www.w3.org/2000/svg" class="icon mb-2 text-danger icon-lg" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"><path stroke="none" d="M0 0h24v24H0z" fill="none"/><path d="M10.24 3.957l-8.422 14.06a1.989 1.989 0 0 0 1.7 2.983h16.845a1.989 1.989 0 0 0 1.7 -2.983l-8.423 -14.06a1.989 1.989 0 0 0 -3.4 0z" /><path d="M12 9v4" /><path d="M12 17h.01" /></svg>
                  <h3>Are you sure?</h3>
                  <div class="text-secondary">Do you really want to remove <span id="DeleteProducTitle">85</span>? What you've done cannot be undone.</div>
                </div>
                <div class="modal-footer">
                  <div class="w-100">
                    <div class="row">
                      <div class="col"><a href="#" class="btn w-100" data-bs-dismiss="modal">
                          Cancel
                        </a></div>
                      <div class="col"><button onclick="deleteCategory()" class="btn btn-danger w-100" >
                          Delete <span id="show_id"></span>

                          {{-- <input type="text" id="id_show"> --}}
                      </button></div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        
    </div>
@endsection

@section('customjs')

{{-- script create --}}
<script>
document.addEventListener("DOMContentLoaded", function() {

Dropzone.autoDiscover = false;
    const dropzone = $("#dropzone-default").dropzone({
        init: function ( ) {
            this.on('addefile',function (file) {
                if(this.files.length > 1 ){
                    this.removeFile(this.files[0]);
                }
            });
        },    
        url: "{{ route('image.create') }}",
        maxFiles : 1,
        paramName : 'image',
        addRemoveLinks:true,
        acceptedFiles:"image/jpeg,image/png,image/gif",
        headers: {
            'X-CSRF-TOKEN': '{{ csrf_token() }}'
        },success:function (file,response) {
            var get_image_id = response['image_id'];
            $("#image_id").val(get_image_id)

            // console.log(get_image_id);
        }
    })
});
$("#categoryForm").submit(function(e) {
    e.preventDefault();
    var element = $(this);
    $.ajax({
        type: "post",
        url: "{{ route('categories.store') }}",
        data: element.serializeArray(),
        dataType: "json",
        success: function(response) {
            var errors = response['errors'];
            if (response['status'] == false) {
                if (errors['name']) {
                    $('#name').addClass('is-invalid')
                        .siblings('p')
                        .addClass('invalid-feedback').html(errors['name']);
                }
                if (errors['slug']) {
                    $('#slug').addClass('is-invalid')
                        .siblings('p')
                        .addClass('invalid-feedback').html(errors['slug']);
                }
                $(document).ready(function() {
                    $("#name").on('input', function() {
                        if ($(this).val().trim() !== '') {
                            $(this).removeClass('is-invalid');
                            $(this).addClass('is-valid');
                        }
                    });
                });
            } else if (response['status'] == true) {
                window.location.href = "{{ route('categories.list') }}"
            }
        },
        error: function(jqXHR, exception) {
            console.log('Something went wrong');
        }
    });
});

$("#name").change(function() {
    element = $(this);
    $.ajax({
        type: "get",
        url: "{{ route('getSlug') }}",
        data: {
            title: element.val()
        },
        dataType: "json",
        success: function(response) {
            if (response["status"] == true) {
                $("#slug").val(response["slug"]).removeClass('is-invalid').addClass('is-valid');
            }
        }
    });
});
</script>
{{-- script update --}}
<script>
document.addEventListener("DOMContentLoaded", function() {

Dropzone.autoDiscover = false;
    const dropzone = $("#dropzone-default-update").dropzone({
        init: function ( ) {
            this.on('addefile',function (file) {
                if(this.files.length > 1 ){
                    this.removeFile(this.files[0]);
                }
            });
        },    
        url: "{{ route('image.create') }}",
        maxFiles : 1,
        paramName : 'image',
        addRemoveLinks:true,
        acceptedFiles:"image/jpeg,image/png,image/gif",
        headers: {
            'X-CSRF-TOKEN': '{{ csrf_token() }}'
        },success:function (file,response) {
            var get_image_id = response['image_id'];
            $("#image_id_update").val(get_image_id)

        }
    })
});

function categoryUpdate(id,name,slug,status,show_to_home,description,image){

    $("#categoryNameUpdate").val(name);
    $("#categorySlugUpdate").val(slug);
    $("#descriptionUpdate").val(description); 
    if(status == 1){
        $("#categoryUpdateStauts").html('<option selected value="1">Active</option><option value="0">Block</option>')
    }
    else {
        $("#categoryUpdateStauts").html('<option selected value="0">Block</option><option value="1">Active</option>')
    }
    if(show_to_home == "Yes" ){
        $("#CategoryUpdateShowToHome").prop("checked",true);
    }
    else {
        $("#CategoryUpdateShowToHome").prop("checked",false);
    }
    var category_image = "/uploads/category/" + image;

    $('#categoryImage').attr('src',category_image);
    
    $("#categoryForm_update").submit(function(e) {
        e.preventDefault();
        var element = $(this);
        $.ajax({
            type: "post",
            url: "/admin/category/" + id + "/update",
            data: element.serializeArray(),
            dataType: "json",
            success: function(response) {
                var errors = response['errors'];
                if (response['status'] == false) {
                    if (errors['name']) {
                        $('#name').addClass('is-invalid')
                            .siblings('p')
                            .addClass('invalid-feedback').html(errors['name']);
                    }
                    if (errors['slug']) {
                        $('#slug').addClass('is-invalid')
                            .siblings('p')
                            .addClass('invalid-feedback').html(errors['slug']);
                    }
                    $(document).ready(function() {
                        $("#name").on('input', function() {
                            if ($(this).val().trim() !== '') {
                                $(this).removeClass('is-invalid');
                                $(this).addClass('is-valid');
                            }
                        });
                    });
                } else if (response['status'] == true) {
                    window.location.href = "{{ route('categories.list') }}";
                }
            },
            error: function(jqXHR, exception) {
                console.log('Something went wrong');
            }
        });
});

}

$("#name").change(function() {
    element = $(this);
    $.ajax({
        type: "get",
        url: "{{ route('getSlug') }}",
        data: {
            title: element.val()
        },
        dataType: "json",
        success: function(response) {
            if (response["status"] == true) {
                $("#slug").val(response["slug"]).removeClass('is-invalid').addClass('is-valid');
            }
        }
    });
});
</script>
<script>
    function get_id(id) {
        $('#show_id').html('#' + id )
        $('#DeleteProducTitle').html('#' + id )
        var del = $("#modal-delete").val(id);
    }   
    function deleteCategory() { 
        var id = $('#modal-delete').val();
        $.ajax({
            type: "get",
            url: "/admin/category/" + id + "/delete",
            data: "data",
            dataType: "json",
            success: function (response) {
                window.location.href = "{{ route('categories.list') }}";
            }
        });
        // var get_id = $(this).data('delete')
        // alert(delete_id);
        // alert(id);
    }
    
</script>
@endsection

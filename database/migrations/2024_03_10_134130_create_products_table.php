<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->integer('SKU');
            $table->string('title');
            $table->string('slug');
            $table->longText('description')->nullable();
            $table->string('weight')->nullable();
            $table->integer('in_stock')->default(1);
            $table->string('image');
            $table->string('price');
            $table->integer('status')->default(1);
            $table->unsignedBigInteger('categoryID');
            $table->unsignedBigInteger('brandID');
            $table->enum('show_to_home',['Yes','No'])->default('No');

            $table->foreign('categoryID')->references('id')->on('categories');
            $table->foreign('brandID')->references('id')->on('brands');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('products');
    }
};

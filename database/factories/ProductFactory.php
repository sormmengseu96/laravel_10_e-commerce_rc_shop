<?php

namespace Database\Factories;

use Faker\Core\Barcode;
use Faker\Core\Uuid;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Validation\Rules\Enum;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Product>
 */
class ProductFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
 
    public function definition(): array
    {
        return [
            'SKU' => rand(100000000,999999999),
            'title'=> fake()->title(),
            'slug' => fake()->name(),
            'description' => fake()->name(),
            'weight' => rand(1,100),
            'in_stock' => rand(1,0),
            'image' => fake()->name(),
            'price' => rand(100,50000),
            'status' => rand(0,1),
            'categoryID' => rand(100,500),
            'brandID' => rand(100,500),
        ];
    }
}

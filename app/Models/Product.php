<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    protected $fillable = [
        'SKU',
        'title',
        'slug',
        'description',
        'weight',
        'in_stock',
        'thumbnail',
        'image',
        'price',
        'status',
        'show_in_home',
        'categoryID',
        'brandID',
    ];
    public function category(){
        return $this->belongsTo(Category::class, 'categoryID');
    }
    public function brand(){
        return $this->belongsTo(Brand::class,'brandID');
    }
}

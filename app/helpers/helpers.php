<?php

use App\Models\Brand;
use App\Models\Category;

function getCategories(){
    return Category::orderBy('name','ASC')->where('show_to_home','Yes')->where('status','1')->get();
}
function getBrands(){
    return Brand::orderBy('name','ASC')->where('show_in_home','Yes')->where('status','1')->get();
}
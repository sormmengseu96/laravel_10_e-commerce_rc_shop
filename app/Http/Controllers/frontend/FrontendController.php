<?php

namespace App\Http\Controllers\frontend;

use App\Http\Controllers\Controller;
use App\Models\Brand;
use App\Models\Category;
use App\Models\Product;
use Illuminate\Http\Request;

class FrontendController extends Controller
{
    public function index(){
        $data = [];
        $product = Product::OrderBy('title','ASC')
                ->where('show_in_home','Yes')
                ->where('status','1')->get();
        $categories = Category::OrderBy('name','ASC')
                ->where('show_to_home','Yes')
                ->where('status','1')->get();
        $data['products'] = $product;
        $data['categories'] = $categories;


        dd($data['products']);
        
            
        return view('frontend.index',$data);
    }
    public function all_product(){
        $data = [];
        $product = Product::all();
        $data['products'] = $product;
        return view('frontend.product.all',$data);
    }
    public function category($id){
        $data = [];
        $categories_of_product = Product::OrderBy('categoryID','ASC')
                                ->where('categoryID','like' ,'%'.$id.'%')
                                ->where('show_in_home','Yes')
                                ->where('status','1')->get();
        $data['products'] = $categories_of_product;
        
        return view('frontend.categories.index',$data);
    }
    public function brand(){
        
        // $data = [];
        // $brand_of_products = Brand::OrderBy('brand','ASC')
        //                     ->where('brandID','like','%'.$id.'%')
        //                     ->where('show_in_home','Yes')
        //                     ->where('status','1')->get();
        // $data['products'] = $brand_of_products;
        $all_brand = Brand::OrderBy('name','ASC')
                    ->where('show_in_home','Yes')
                    ->where('status','1')->get();
        
        return view('frontend.brand.index',compact('all_brand'));

    }
}

<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Models\Brand;
use App\Models\Image;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class BrandController extends Controller
{
    public function index(Request $request){
        
        $brands = Brand::latest(); 

        if(!empty($request->keyword)){
            $brands = Brand::where('name','like','%'.$request->keyword.'%');
        }

        $brands = $brands->paginate(10);
        return view('admin.brand.index',compact('brands'));
    }
    public function show($id){
        $brand = Brand::find($id);
        return view('admin.brand.show',compact('brand'));
    }
    public function create(){
        return view('admin.brand.create');
    }
    public function store(Request $request){
        
        $validator = Validator::make($request->all(),[

            'name' => 'required',
            'slug' => 'required|unique:brands',

        ]);

        if($validator->passes()){

            $brand = new Brand();
            $brand->name = $request->name;
            $brand->slug = $request->slug;
            $brand->status = $request->status;       
            $request->show_to_home == 'On' ? $brand->show_to_home = 'Yes' : $brand->show_to_home = 'No';
            $brand->description = $request->description;
            $brand->save();

            if(!empty($request->image_id)){
                
                $Image = Image::find($request->image_id);

                $extArray = explode('.',$Image->name);
                $ext = last($extArray);


                $newImageName = $brand->id .'.'.$ext;
                $sPath = public_path().'/temp/'.$Image->name;
                $dPath = public_path().'/uploads/brands/'.$newImageName;
                File::copy($sPath,$dPath);

                $brand->logo =  $newImageName;
                $brand->save();

            }
            Session::flash('success','Brand created successfully');
            return response()->json([
                'status' => true,
                'message' => "brand create success",
            ]);

        } else{
            
            return response()->json([
                'status' => false,
                'errors' => $validator->errors(),
            ]);

        }   
    }
    public function edit($brand_id){
        $brand = Brand::find($brand_id);
        return view('admin.brand.edit',compact('brand'));
    }
    public function update(Request $request,$brand_id){
        $brand = Brand::find($brand_id);

        if(empty($brand)){
           return response()->json([
                'status' => false,
                'Not found' => true,
                'message' => 'brand not found !'
           ]);
        };

        $validator = Validator::make($request->all(),[
            'name' => 'required',
            'slug' => 'required|unique:brands,slug,'.$brand->id.',id',
            // 'show_in_home' => 'required'
        ]);

        if($validator->passes()){
            if(!empty($request->image_id)){
                $Image = Image::find($request->image_id);
    
                $extArray = explode('.',$Image->name);
                $ext = last($extArray);
    
                $newImageName = $brand->id .'.'.$ext;
                $sPath = public_path().'/temp/'.$Image->name;
                $dPath = public_path().'/uploads/brands/'.$newImageName;
                File::copy($sPath,$dPath);
    
                $brand->logo =  $newImageName;

            }
            
            $brand->name = $request->name;
            $brand->slug = $request->slug;
            $brand->description = $request->description;
            $brand->status = $request->status;
            $request->show_to_home == "on" ? $brand->show_to_home = 'Yes' : $brand->show_to_home = 'No';
            $brand->save();
            Session::flash('success','Brand update successfully');
            return response()->json([
                'status' => true,
                'message' => 'Brand update success',
            ]);

        } else {
            return response()->json([
                'status' => false,
                'errors' => $validator->errors()
            ]);
        }

    }
    public function delete($id){
        
        $brand = Brand::find($id);

        if(!empty($brand)){

            $brand->delete();

            return response()->json([
                'status' => true,
                'id' => $id,
                'message' => 'brand delete success',
            ]);

        } else {

            return response()->json([
                'status' => false,
                'not found' => true,
                'message' => 'brand not found !'

            ]);
        }
        
    }
}

<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Models\Brand;
use App\Models\Category;
use App\Models\Image;
use App\Models\Product;
use DOMDocument;
use Exception;
use Generator;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class productController extends Controller
{
    public function index(Request $request){
        $products = Product::latest();
        if(!empty($request->keyword)){
            $products = Product::where('name','like' , '%'. $request->keyword. '%');
        }
        $products = $products->paginate(10);

        // Redirect back to the previous page or to a specific route
        // return redirect()->back();
        return view('admin.product.index',compact('products'));
    }
    
    public function show($id){
        $data = [];
        $product = Product::find($id);
        $categories = Category::find($product->categoryID);
        $brands = Brand::find($product->brandID);
        $data['products'] = $product;
        $data['brands'] = $brands;
        $data['categories'] = $categories;
        return view('admin.product.show',$data);
    }

    public function create(){

        $data = [];
        $categories = Category::all();
        $brands = brand::all();
        $data['categories'] = $categories;
        $data['brands'] = $brands; 

        return view('admin.product.create',$data);
    }
    public function store(Request $request){
        $rule = [
            
            'title' => 'required',
            'slug' => 'required|unique:products',
            'status' => 'required',
            'sku' => 'required',
            'category' => 'required',    
            'brand' => 'required', 
            'stock' => 'required|numeric',
            'price' => 'required|numeric',
            'weight' => 'required|numeric',
            'category' => 'required',
            'brand' => 'required',
        ];        

        $validator = Validator::make($request->all(),$rule);
        // Generator barcode 
        $number = mt_rand(100000000,999999999);
        if($this->productCodeExists($number)){
            $number = mt_rand(100000000,999999999);
        }
        
        if($validator->passes()){

            $product = new Product();
            $product->title = $request->title;
            $product->slug = $request->slug;
            $product->weight = $request->weight;
            $product->categoryID = $request->category;
            $product->brandID = $request->brand;
            $product->in_stock = $request->stock;
            $product->price = $request->price;
            $product->sku = $request->sku;
            $product->status = $request->status;
            $product->description = $request->description;
            $product->save();
            if(!empty($request->image_id)){

                $Image = Image::find($request->image_id);

                $extArray = explode('.',$Image->name);
                $ext = last($extArray);

                $newImageName = $product->id .'.'.$ext;
                $sPath = public_path().'/temp/'.$Image->name;
                $dPath = public_path().'/uploads/product/'.$newImageName;
                File::copy($sPath,$dPath);

                $product->image =  $newImageName;
                $product->save();

            }
            Session::flash('success', 'Product has been create successfully.');
            return response()->json([
                'status' => true,
                'message' => 'add success',
            ]);
        } else {

            return response()->json([
                'status' => false,
                'errors' => $validator->errors(),
            ]);
        }

    }   

    public function productCodeExists($number){

        return product::whereProductCode($number);

    }

    public function edit($product_id){
        $data = [];
        $product = Product::find($product_id);        

        $show_categoryID = Category::find($product->categoryID);
        $show_brandID = Brand::find($product->brandID);
        $data = [
            'product' => $product,
            'categories' => Category::all()->where('status','1'),
            'brands' => Brand::all()->where('status','1'),

        ];
        // return response()->json($categoryID);
        
        return view('admin.product.edit',$data);
    }

    public function update(Request $request,$product_ID){
        $rule = [
            
            'title' => 'required',
            'slug' => 'required',
            'status' => 'required',
            'sku' => 'required|numeric',
            'category' => 'required',    
            'brand' => 'required', 
            'stock' => 'required',
            'price' => 'required|numeric',
            'weight' => 'required|numeric',
        ];        
        $validator = Validator::make($request->all(),$rule);
        if($validator->passes()){

            $product = Product::find($product_ID);
            $product->title = $request->title;
            $product->slug = $request->slug;
            $product->weight = $request->weight;
            $product->categoryID = $request->category;
            $product->brandID = $request->brand;
            $product->price = $request->price;
            $product->in_stock = $request->stock;
            $product->status = $request->status;
            $request->show_to_home == 'on' ? $product->show_to_home = 'Yes' : $product->show_to_home = 'No';
            $product->description = $request->description;

            // $description = $request->description;
            
            // if(!empty($description)){
                
            //     $dom = new DOMDocument();
            //     $dom->loadHtml($description, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
            //     $images = $dom->getElementsByTagName('img');

            //     foreach ($images as $key => $img) {
            //         $data = $img->getAttribute('src');
            //         list($type, $data) = explode(';', $data);
            //         list(, $data)      = explode(',', $data);
            //         $imgeData = base64_decode($data);
            //         $image_name= "/uploads/product_decriptions/" . time().$key.'.png';
            //         $path = public_path() . $image_name;
            //         file_put_contents($path, $imgeData);
                    
            //         $img->removeAttribute('src');
            //         $img->setAttribute('src', $image_name);

            //     }
            //     $description = $dom->saveHTML();
            //     $product->description = $description;

            // }
            
            if(!empty($request->image_id)){

                $Image = Image::find($request->image_id);

                $extArray = explode('.',$Image->name);
                $ext = last($extArray);

                $newImageName = $product->id .'.'.$ext;
                $sPath = public_path().'/temp/'.$Image->name;
                $dPath = public_path().'/uploads/product/'.$newImageName;
                File::copy($sPath,$dPath);

                $product->image =  $newImageName;
                $product->save();

            }

            $product->update();
            Session::flash('success', 'Product has been update successfully.');

            return response()->json([
                'status' => true,
                'message' => 'Products Update success',
        
            ]);

        } else {

            return response()->json([
                'status' => false,
                'errors' => $validator->errors(),
            ]);

        }

    }

    public function delete($id){

        $product = Product::find($id);

        if(!empty($product)){
            $product->delete();
            Session::flash('danger','Product has been delete !');
            return response()->json([
                'status' => true,
                'message' => 'product delete successful !'
            ]);
        } else {
            return response()->json([
                'status' => true,
                'error' => 'product not found !'
            ]);
        }

    }
}

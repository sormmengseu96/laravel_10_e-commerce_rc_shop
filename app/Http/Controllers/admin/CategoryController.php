<?php

namespace App\Http\Controllers\admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Models\Category;
use App\Models\Image;
// use Illuminate\Contracts\Session\Session;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Session;
use Response;
class CategoryController extends Controller
{
    public function index(Request $request){
        $categories = Category::latest();
        if(!empty($request->keyword)){

            $categories = Category::where('name','like','%'.$request->keyword.'%');
        }
        $categories = $categories->paginate(10);
        return view('admin.category.list',compact('categories'));
    }
    public function show($id){
        $category = Category::find($id);
        return view('admin.category.show',compact('category'));
    }
    public function create(){
        
        return view('admin.category.create');

    }
    public function store(Request $request){
        $validator = Validator::make($request->all(),[
            'name' => 'required',
            'slug' => 'required|unique:categories',
            'status' => 'required',
        ]);

        if($validator->passes()){
            $category = new Category();
            $category->name = $request->name;
            $category->slug = $request->slug;
            $category->description = $request->description;
            $category->status = $request->status;
            $request->show_to_home == 'on' ?  $category->show_to_home = "Yes" : $category->show_to_home = "No" ; 
            $category->save();  

            if(!empty($request->image_id)){
                $Image = Image::find($request->image_id);

                $extArray = explode('.',$Image->name);
                $ext = last($extArray);

                $newImageName = $category->id .'.'.$ext;
                $sPath = public_path().'/temp/'.$Image->name;
                $dPath = public_path().'/uploads/category/'.$newImageName;
                File::copy($sPath,$dPath);

                $category->image =  $newImageName;
                $category->save();
            }
            Session::flash('success','Category has been successfully');
            return response()->json([
                'status' => true,
                'message' => 'Category add successfull',
            ]);
        }else{
            return response()->json([
                'status' => false,
                'errors' =>  $validator->errors()
            ]);
        }
    }
    public function edit($id){
        $category = Category::find($id);
        return view('admin.category.edit',compact('category'));
    }
    public function update(Request $request,$id){
        $category = Category::find($id);
        if(empty($category)){
            return response()->json([
                'status' => false,
                'notFound' => true,
                'message' => 'Category not found',
            ]);
        }
        $validator = Validator::make($request->all(),[
            'name' => 'required',
            'slug' => 'required|unique:categories,slug,'.$category->id.',id',
        ]);
        if($validator->passes()){
            if(!empty($request->image_id)){
                
                $Image = Image::find($request->image_id);
    
                $extArray = explode('.',$Image->name);
                $ext = last($extArray);
    
                $newImageName = $category->id .'.'.$ext;
                $sPath = public_path().'/temp/'.$Image->name;
                $dPath = public_path().'/uploads/category/'.$newImageName;
                File::copy($sPath,$dPath);
                $category->image =  $newImageName;
            }
            
            
            $category->name = $request->name;
            $category->slug = $request->slug;
            $category->status = $request->status;
            $request->show_to_home == "on" ? $category->show_to_home = "Yes" :  $category->show_to_home = "No";
            $category->description = $request->description;
            $category->update();

            Session::flash('success','Category update successfully');
            return response()->json([
                'status' => true,
                'message' => "update success",
            ]);

        }else{
            return response()->json([
                'status' => false,
                'errors' => $validator->errors(),
            ]);
        }

    }
    public function delete($id){
        // return response()->json($id);
        $category = Category::find($id);
        
        if($category->delete()){
            Session::flash('danger','Category has been delele');
            return response()->json([
                'status' => true,
                'message' => $id,
            ]);  
        }else{
            return response()->json([
                'status' => false,
                'message' => 'Category not found !',
            ]);
        }

    }
  
}
    
  
<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Models\Image;
use Illuminate\Http\Request;

class ImageController extends Controller
{
    public function create(Request $request){
        $get_image = $request->image;

        if(!empty($get_image)){
            $ext = $get_image->getClientOriginalExtension();
            
            $newName = time().'.'.$ext;
            
            $Image = new Image();
            $Image->name = $newName;
            $Image->save();

            $get_image->move(public_path().'/temp',$newName);

            return response()->json([
                'status' => true,
                'image_id' => $Image['id'],
                'message' => 'Image add successfully'
            ]);
        }
    }
}

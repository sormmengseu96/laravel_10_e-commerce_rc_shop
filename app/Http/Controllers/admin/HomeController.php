<?php

// namespace App\Http\Controllers;
namespace App\Http\Controllers\admin;
use Illuminate\Http\Request;
Use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{

    public function index(){
        // $admin = Auth::guard('admin')->user();
        // echo 'Welcome'.$admin->name. '<a href="'.route('admin.logout').'">logout</a>';
        return view('admin.dashboard');
    }
    public function logout(){
        Auth::guard('admin')->logout();
        return redirect()->route('admin.login');
    }
}

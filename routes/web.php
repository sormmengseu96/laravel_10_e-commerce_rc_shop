<?php

use App\Http\Controllers\admin\AdminLoginController;
use App\Http\Controllers\admin\BrandController;
use App\Http\Controllers\admin\CategoryController;
use App\Http\Controllers\admin\HomeController;
use App\Http\Controllers\admin\ImageController;
use App\Http\Controllers\admin\productController;
use App\Http\Controllers\frontend\FrontendController;
use App\Models\Product;
use GuzzleHttp\Middleware;
use Illuminate\Http\Request;
use Illuminate\Routing\RouteRegistrar;
use Illuminate\Support\Facades\Route;
use Laravel\Ui\Presets\React;
use Illuminate\Support\Str;
use Livewire\Component;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/test', function () {
    return view('welcome');
});




// Route::get('/admin.login',[AdminLoginController::class,'index'])->name('login');

// Route::get('/',[FrontendController::class,'index' ])->name('front.index');
Route::get('/all_product',[FrontendController::class,'all_product'])->name('frontend.products');
Route::get('/categories/{id}',[FrontendController::class,'category'])->name('frontend.categorie');
Route::get('/brands',[FrontendController::class,'brand'])->name('frontend.brands');





Route::group(['prefix'=> 'admin'],function(){

    Route::group(['middleware' => 'admin.guest'],function(){

        Route::get('/login',[AdminLoginController::class,'index'])->name('admin.login');
        Route::post('/authenticate',[AdminLoginController::class,'authenticate'])->name('admin.authenticate');

    });
    Route::group(['middleware' => 'admin.auth'],function(){
        Route::get('/dashboard',[HomeController::class,'index'])->name('admin.dashboard');
        Route::get('/logout',[HomeController::class,'logout'])->name('admin.logout');
        
        #route category
        Route::get('/categories/list',[CategoryController::class,'index'])->name('categories.list');
        Route::get('/categories/{category}/show',[CategoryController::class,'show'])->name('categories.show');
        Route::get('/categories/create',[CategoryController::class,'create'])->name('categories.create');
        Route::post('/categories/store',[CategoryController::class,'store'])->name('categories.store');
        Route::get('/category/{category}/edit',[CategoryController::class,'edit'])->name('categories.edit');
        Route::post('/category/{category}/update',[CategoryController::class,'update'])->name('categories.update');
        Route::get('/category/{category}/delete',[CategoryController::class,'delete'])->name('categories.delete');
        //image upload
        Route::post('/image/upload',[ImageController::class,'create'])->name('image.create');
        
        Route::get('/getSlug',function(Request $request){
            $slug = '';
            if(!empty($request->title)){
                $slug = Str::slug($request->title);                
            }
            return response()->json([
                'status' => true,
                'slug' => $slug
            ]);
        })->name('getSlug');

        //brand
        Route::get('/brands',[BrandController::class, 'index'])->name('brands.index');
        Route::get('/brand/{brand}/show',[BrandController::class, 'show'])->name('brands.show');
        Route::get('/brand/create',[BrandController::class, 'create'])->name('brand.create');
        Route::post('/brand/store',[BrandController::class, 'store'])->name('brand.store');
        Route::get('/brand/{brand}/edit',[BrandController::class,'edit' ])->name('brand.edit');
        Route::post('/brand/{brand}/update',[BrandController::class,'update' ])->name('brand.update');
        Route::get('/brand/{brand}/delete',[BrandController::class,'delete' ])->name('brand.delete');

        //product 
        Route::get('/products',[productController::class,'index'])->name('products.index');
        Route::get('/product/{product}/show',[productController::class,'show'])->name('products.show');
        Route::get('/products/create',[productController::class,'create'])->name('products.create');
        Route::post('/product/store',[productController::class,'store'])->name('product.store');
        Route::get('/product/{product}/edit',[productController::class, 'edit'])->name('product.edit');
        Route::post('/product/{product}/update',[productController::class, 'update'])->name('product.update');
        Route::get('/product/{product}/delete',[productController::class, 'delete'])->name('product.delete');
        // Route::get('/product/keyword/')
        
    });
    
});
